package de.niheu.warenhaus.interfaces;

import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.infrastructure.customer.CustomerFilterType;
import de.niheu.warenhaus.infrastructure.customer.CustomerRepository;
import de.niheu.warenhaus.infrastructure.customer.CustomerSortType;
import de.niheu.warenhaus.infrastructure.filter.FilterFactory;
import de.niheu.warenhaus.infrastructure.sort.SortFactory;
import de.niheu.warenhaus.infrastructure.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import java.util.Comparator;
import java.util.function.Predicate;


@ShellComponent
public class CustomerCLI {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerCLI(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * Lists all customers based on the provided search patterns and sorts them accordingly.
     *
     * @param page          The page number.
     * @param size          The page size.
     * @param searchPatterns The search patterns to filter customers. Each pattern should be in the format "attribute:pattern".
     *                       Multiple patterns can be separated by semicolon.
     *                       Example: "name:John;email:example.com"
     * @param sortParam     The sorting parameter in the format "direction:field1;field2;field3".
     *                       Direction can be "asc" for ascending or "desc" for descending.
     *                       Example: "asc:name;email"
     * @return A formatted string representing the list of customers.
     * E.g. list-customers 0 10 name:na ASC:name
     */
    @ShellMethod("List all customers")
    public String listCustomers(@ShellOption(defaultValue = "0") int page,
                                @ShellOption(defaultValue = "10") int size,
                                @ShellOption(defaultValue = "") String searchPatterns,
                                @ShellOption(defaultValue = "") String sortParam) {
        Predicate<Customer> filters = FilterFactory.createFilters(new CustomerFilterType(), searchPatterns);

        SortOrder sortOrder = CustomerCLIUtils.parseSortOrder(sortParam);
        String[] sortFields = CustomerCLIUtils.parseSortFields(sortParam);
        Comparator<Customer> comparator = SortFactory.createComparator(new CustomerSortType(), sortOrder, sortFields);

        Page<Customer> customerPage = customerRepository.findAll(PageRequest.of(page, size), filters, comparator);
        return CustomerCLIUtils.formatCustomerPage(customerPage, page);
    }
}
