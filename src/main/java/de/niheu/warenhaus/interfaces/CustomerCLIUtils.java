package de.niheu.warenhaus.interfaces;

import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.infrastructure.sort.SortOrder;
import org.springframework.data.domain.Page;

public class CustomerCLIUtils {

    public static SortOrder parseSortOrder(String sortParam) {
        SortOrder sortOrder = SortOrder.NONE;
        String[] sortArgs = sortParam.split("[:;]");
        if (sortArgs.length > 0 && !sortArgs[0].isEmpty()) {
            sortOrder = SortOrder.fromString(sortArgs[0]);
        }
        return sortOrder;
    }

    public static String[] parseSortFields(String sortParam) {
        String[] sortArgs = sortParam.split("[:;]");
        String[] sortFields = new String[sortArgs.length - 1];
        System.arraycopy(sortArgs, 1, sortFields, 0, sortFields.length);
        return sortFields;
    }

    public static String formatCustomerPage(Page<Customer> customerPage, int page) {
        StringBuilder sb = new StringBuilder();
        sb.append("Page ").append(page + 1).append(" of ").append(customerPage.getTotalPages()).append("\n");
        for (Customer customer : customerPage.getContent()) {
            sb.append(customer).append("\n");
        }
        return sb.toString();
    }
}

