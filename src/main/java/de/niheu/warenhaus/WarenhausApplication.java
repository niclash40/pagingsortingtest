package de.niheu.warenhaus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarenhausApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarenhausApplication.class, args);
	}

}
