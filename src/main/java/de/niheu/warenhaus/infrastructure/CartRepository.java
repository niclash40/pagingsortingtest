package de.niheu.warenhaus.infrastructure;

import de.niheu.warenhaus.core.Cart;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class CartRepository extends PagingRepository<Cart> {
    public void save(Cart cart) {
        cacheData.add(cart);
    }

    public void saveAll(List<Cart> cartList) {
        cacheData.addAll(cartList);
    }
}