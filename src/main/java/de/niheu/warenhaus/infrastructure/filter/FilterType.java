package de.niheu.warenhaus.infrastructure.filter;

import java.util.function.Predicate;

public interface FilterType<T> {
    Predicate<T> createFilter(String attribute, String pattern);
}
