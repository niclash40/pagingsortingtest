package de.niheu.warenhaus.infrastructure.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class FilterFactory {

    public static <T> Predicate<T> createFilters(FilterType<T> filterType, String... searchPatterns) {
        List<Predicate<T>> filters = new ArrayList<>();
        for (String searchPattern : searchPatterns) {
            String[] parts = searchPattern.split(":");
            String pattern = parts.length > 1 ? parts[1] : "";

            filters.add(filterType.createFilter(parts[0], pattern));
        }
        return filters.stream().reduce(Predicate::and).orElse(t -> true);
    }
}
