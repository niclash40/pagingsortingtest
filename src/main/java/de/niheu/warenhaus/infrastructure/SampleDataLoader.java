package de.niheu.warenhaus.infrastructure;

import de.niheu.warenhaus.core.Cart;
import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.core.Item;
import de.niheu.warenhaus.infrastructure.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Order(-1)
@Component
public class SampleDataLoader implements CommandLineRunner {

    private final CustomerRepository customerRepository;
    private final ItemRepository itemRepository;
    private final CartRepository cartRepository;

    @Autowired
    public SampleDataLoader(CustomerRepository customerRepository, ItemRepository itemRepository, CartRepository cartRepository) {
        this.customerRepository = customerRepository;
        this.itemRepository = itemRepository;
        this.cartRepository = cartRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Customer> customers = new ArrayList<>();
        List<String> names = nameGenerator(100);
        for (int i = 1; i <= 100; i++) {
            String name = names.get(i-1) + i;
            String email = names.get(i-1) + i + "@example.com";
            String address = "Address " + i;
            String phoneNumber = "123456789" + i;
            Date createdAt = new Date();
            Date lastLogin = new Date();
            customers.add(new Customer(i, name, email, address, phoneNumber, createdAt, lastLogin));
        }
        customerRepository.saveAll(customers);

        Item item1 = new Item(1, "Smartphone", "High-quality smartphone", 799.99, 100, new Date(), new Date());
        Item item2 = new Item(2, "Laptop", "Powerful laptop for professionals", 1299.99, 50, new Date(), new Date());
        itemRepository.saveAll(List.of(item1, item2));

        List<Item> itemsInCart = new ArrayList<>();
        itemsInCart.add(item1);
        itemsInCart.add(item2);
        Cart cart1 = new Cart(1, customers.get(0).customerId(), itemsInCart, new Date(), new Date());
        cartRepository.save(cart1);
    }

    private static List<String> nameGenerator(int count) {
        List<String> names = new ArrayList<>();
        names.add("Alexander");
        names.add("Maximilian");
        names.add("Paul");
        names.add("Leon");
        names.add("Finn");
        names.add("Luca");
        names.add("Jonas");
        names.add("Elias");
        names.add("Felix");
        names.add("Nico");
        names.add("Anna");
        names.add("Emma");
        names.add("Lea");
        names.add("Hannah");
        names.add("Lena");
        names.add("Marie");
        names.add("Lara");
        names.add("Sophie");
        names.add("Lina");
        names.add("Mia");
        names.add("Julia");

        while (names.size() < count) {
            names.addAll(names);
        }

        return names.subList(0, count);
    }
}

