package de.niheu.warenhaus.infrastructure.sort;

public enum SortOrder {
    ASC, DESC, NONE;

    public static SortOrder fromString(String sortOrder) {
        switch (sortOrder.toUpperCase()) {
            case "ASC":
                return ASC;
            case "DESC":
                return DESC;
            default:
                return NONE;
        }
    }
}

