package de.niheu.warenhaus.infrastructure.sort;

public class SortNotFoundException extends RuntimeException {
    public SortNotFoundException(String msg) {
        super(msg);
    }
}
