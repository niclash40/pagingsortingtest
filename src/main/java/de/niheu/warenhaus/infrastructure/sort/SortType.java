package de.niheu.warenhaus.infrastructure.sort;

import java.util.List;
import java.util.function.Function;

public interface SortType<T> {
    List<Function<T, Comparable<?>>> getSortFunctions(String... sortCriteria);
}
