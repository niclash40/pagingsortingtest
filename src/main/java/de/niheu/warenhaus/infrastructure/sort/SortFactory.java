package de.niheu.warenhaus.infrastructure.sort;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class SortFactory {

    public static <T> Comparator<T> createComparator(SortType<T> sortType, SortOrder sortOrder, String... sortCriteria) {
        List<Function<T, Comparable<?>>> functions = sortType.getSortFunctions(sortCriteria);
        return (o1, o2) -> compareObjects(o1, o2, functions, sortOrder);
    }

    private static <T> int compareObjects(T o1, T o2, List<Function<T, Comparable<?>>> functions, SortOrder sortOrder) {
        if (sortOrder == SortOrder.NONE || functions.isEmpty()) {
            return 0; // Keine Sortierung oder keine Sortierfunktionen, daher keine Änderung der Reihenfolge
        }

        for (Function<T, Comparable<?>> function : functions) {
            int cmp = compareValues((Comparable<Object>) function.apply(o1), (Comparable<Object>) function.apply(o2));
            if (cmp != 0) {
                return sortOrder == SortOrder.ASC ? cmp : -cmp;
            }
        }

        return 0;
    }

    private static int compareValues(Comparable<Object> c1, Comparable<Object> c2) {
        return c1.compareTo(c2);
    }
}