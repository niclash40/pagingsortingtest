package de.niheu.warenhaus.infrastructure;

import de.niheu.warenhaus.core.Item;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class ItemRepository extends PagingRepository<Item>{
    public void save(Item item) {
        cacheData.add(item);
    }

    public void saveAll(List<Item> itemList) {
        cacheData.addAll(itemList);
    }
}

