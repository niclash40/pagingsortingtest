package de.niheu.warenhaus.infrastructure.customer;

import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.infrastructure.sort.SortNotFoundException;
import de.niheu.warenhaus.infrastructure.sort.SortType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CustomerSortType implements SortType<Customer> {
    @Override
    public List<Function<Customer, Comparable<?>>> getSortFunctions(String... sortCriteria) {
        List<Function<Customer, Comparable<?>>> functions = new ArrayList<>();
        for (String sortCriterion : sortCriteria) {
            switch (sortCriterion.toUpperCase()) {
                case "NAME":
                    functions.add(Customer::name);
                    break;
                case "EMAIL":
                    functions.add(Customer::email);
                    break;
                case "PHONE":
                    functions.add(Customer::phoneNumber);
                    break;
                default:
                    throw new SortNotFoundException("SortCriteria " + sortCriterion + " not found");
            }
        }
        return functions;
    }
}
