package de.niheu.warenhaus.infrastructure.customer;

import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.infrastructure.PagingRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class CustomerRepository extends PagingRepository<Customer> {
    public void save(Customer customer) {
        cacheData.add(customer);
    }

    public void saveAll(List<Customer> customerList) {
        cacheData.addAll(customerList);
    }
}
