package de.niheu.warenhaus.infrastructure.customer;

import de.niheu.warenhaus.core.Customer;
import de.niheu.warenhaus.infrastructure.filter.FilterType;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class CustomerFilterType implements FilterType<Customer> {

    @Override
    public Predicate<Customer> createFilter(String attribute, String pattern) {
        switch (attribute.toUpperCase()) {
            case "NAME":
                return customer -> {
                    Pattern regexPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
                    return regexPattern.matcher(customer.name()).find();
                };
            case "EMAIL":
                return customer -> {
                    Pattern regexPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
                    return regexPattern.matcher(customer.email()).find();
                };
            default:
                return customer -> true; // No filter found, so no filter will use
        }
    }
}