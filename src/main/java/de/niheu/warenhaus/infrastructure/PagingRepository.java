package de.niheu.warenhaus.infrastructure;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class PagingRepository<T> {
    protected final List<T> cacheData = new ArrayList<>();

    public Page<T> findAll(Pageable pageable, Predicate<T> filter, Comparator<T> comparator) {
        List<T> filteredAndSortedData = filterAndSortData(filter, comparator);
        return paginateData(pageable, filteredAndSortedData);
    }

    private List<T> filterAndSortData(Predicate<T> filter, Comparator<T> comparator) {
        return cacheData.stream().filter(filter).sorted(comparator).collect(Collectors.toList());
    }

    private Page<T> paginateData(Pageable pageable, List<T> data) {
        int totalItems = data.size();
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), totalItems);

        if (start > end) {
            return new PageImpl<>(List.of(), pageable, 0);
        }

        List<T> sublist = data.subList(start, end);
        return new PageImpl<>(sublist, pageable, totalItems);
    }
}

