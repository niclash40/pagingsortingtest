package de.niheu.warenhaus.core;

import java.util.Date;

public record Customer(int customerId, String name, String email, String address, String phoneNumber, Date createdAt, Date lastLogin) {
}
