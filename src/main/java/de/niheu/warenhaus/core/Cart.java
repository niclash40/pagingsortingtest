package de.niheu.warenhaus.core;

import java.util.Date;
import java.util.List;

public record Cart(int cartId, int customerId, List<Item> items, Date createdAt, Date updatedAt) {
}