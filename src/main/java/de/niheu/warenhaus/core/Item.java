package de.niheu.warenhaus.core;

import java.util.Date;

public record Item(int itemId, String name, String description, double price, int stockQuantity, Date createdAt, Date updatedAt) {
}